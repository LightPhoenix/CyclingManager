<?php

use Illuminate\Database\Seeder;
use App\Models\Subdomain;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $oSubdomain = new Subdomain();
        $oSubdomain->name = "LRTV Swift";
        $oSubdomain->subDomain = "swift";
        $oSubdomain->save();
    }
}
