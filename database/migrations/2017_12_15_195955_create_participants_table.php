<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('from');
            $table->integer('raceNumber')->default(0);
            $table->integer('position')->default(0);
            $table->boolean('dnf')->default(false);
            $table->boolean('dns')->default(false);
            $table->boolean('dq')->default(false);
            $table->boolean('raceDone')->default(false);
            $table->integer('category_id')->nullable();
            $table->boolean('trash')->default(false);
            $table->boolean('manual')->default(false);
            $table->string('hash'); // name + from -> md5
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant');
    }
}
