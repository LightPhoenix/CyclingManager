@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Filter</div>

            <div class="panel-body">
                <div class="row">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="col-md-3">
                        {{Form::select('year', $aYear, date('Y'), ['class' => 'form-control', 'id' => 'txtYear'])}}
                    </div>
                    <div class="col-md-3">
                        {{Form::select('month', $aMonth, date('m'), ['class' => 'form-control', 'id' => 'txtMonth'])}}
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-success" id="filterRaces">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Races</div>

                    <div class="panel-body" id="raceData">
                        <img class='spinner' width='200' height='200' src='/images/loading_spinner.gif'>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
