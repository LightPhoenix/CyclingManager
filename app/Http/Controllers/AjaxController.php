<?php

namespace App\Http\Controllers;

use App\Models\Race;
use App\Models\Subdomain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buildRoutes() {

        $aMethods = get_class_methods($this);
        $aActionMethods = preg_grep("/^action_/", $aMethods);

        foreach ($aActionMethods as $row)
        {
            $sUrlName = substr($row, 7, (strlen($row) - 7));
            Route::any('/ajax/' . $sUrlName, ['uses' => 'AjaxController@' . $row]);

        }
    }

    public function action_getRaces(Request $request) {

        if($request->has('year') && $request->has('month')) {
            $year = $request->input('year');
            $month = $request->input('month');

            $races = Race::whereRaw('YEAR(startDate) = ' . $year . ' AND MONTH(startDate) = ' . $month)->orderBy('startDate')->get();
            $hashList = Subdomain::getHashList();

            foreach ($races as $race) {
                $race->categories = $race->categories();

                foreach ($race->categories as $category) {
                    $category->participants = $category->participantsWithHash($hashList);
                }

            }

            $return = new \stdClass();
            $return->hashes = $hashList;
            $return->result = $races;

            echo json_encode($return);
        }


    }

}
