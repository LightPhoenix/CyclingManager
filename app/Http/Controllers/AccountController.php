<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oUser = \Auth::user();

        $oHashList = Participant::where('name', 'LIKE', '%' . $oUser->name . '%')->distinct('hash')->get(['name', 'from', 'hash']);

        return view('account.edit', ['aData' => $oHashList, 'oUser' => $oUser]);
    }

    public function post() {

        if(Input::has('racerHash')) {
            $oUser = \Auth::user();
            $oUser->racerHash = Input::get('racerHash');
            $oUser->save();

            return back()
                ->with('message-success', 'Cyclist attachment successful.');
        } else {
            return back()
                ->with('message-error', 'Please select a cyclist.');
        }

    }
}
