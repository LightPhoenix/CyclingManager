<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Models\Race;
use App\Models\Category;
use App\Models\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;

class ImportController extends Controller
{

    public function importRace($year, $month) {

        Log::info('Import race (year: ' . $year . ' month: ' . $month . ')');

        $sUrl = 'http://api.knwu.nl/?callback=&handle=knwu.app.calendar.get&jaar=' . $year . '&maand=' . $month;

        if(Cache::sameAsCache($sUrl)) { //Item is exact gelijk als de vorige import, we hoeven niets te doen
            Log::info('> Skip');
            return;
        }

        $json = file_get_contents($sUrl);
        $decoded = json_decode($json);

        //Alles wat al in de database staat in de prullenbak stoppen, hierdoor kunnen we items verwijderen die niet langer in de import meekomen
        DB::update("UPDATE race SET `trash` = 1 WHERE MONTH(startDate) = ? AND YEAR(startDate) = ? AND `manual` = 0", [$month, $year]);

        foreach ($decoded->result as $race) {

            $oRace = Race::firstOrNew(['id' => (int)$race->id]);
            $oRace->id = (int)$race->id;
            $oRace->name = $race->naam;
            $oRace->location = $race->plaats;
            $oRace->startDate = $race->startdatum;
            $oRace->permanence = $race->permanence;
            $oRace->discipline = $race->discipline;
            $oRace->trash = false;
            $oRace->isCanceled = (boolean)$race->isGeannuleerd;
            $oRace->save();

            /* Import categorieen, bijv amateur/sportklasse */
            DB::update("UPDATE category SET `trash` = 1 WHERE `race_id` = ? AND `manual` = 0", [$oRace->id]);

            if(isset($race->categorieen)) {

                foreach ($race->categorieen as $category) {
                    $oCategory = Category::firstOrNew(['id' => (int)$category->categorieId]);
                    $oCategory->id = (int)$category->categorieId;
                    $oCategory->name = $category->catNaam;
                    $oCategory->date = $category->datum;
                    $oCategory->time = $category->tijd;
                    $oCategory->race_id = $oRace->id;
                    $oCategory->participantsFrom = $category->indeling;
                    $oCategory->raceType = $category->soort;
                    $oCategory->trash = false;
                    $oCategory->save();


                    DB::update("UPDATE participant SET `trash` = 1 WHERE `category_id` = ? AND `manual` = 0", [$oCategory->id]);

                    /* Er zijn deelnemers */
                    if (isset($category->deelnemers)) {
                        /* Deelnemers */
                        foreach ($category->deelnemers as $participant) {
                            $sNewHash = Participant::generateHash($participant->naam . $participant->woonplaats);

                            $oParticipant = Participant::firstOrNew(['hash' => $sNewHash, 'category_id' => $oCategory->id]);
                            $oParticipant->raceNumber = (int)$participant->rugnummer;
                            $oParticipant->name = $participant->naam;
                            $oParticipant->category_id = $oCategory->id;
                            $oParticipant->from = $participant->woonplaats;
                            $oParticipant->trash = false;
                            $oParticipant->hash = $sNewHash;
                            $oParticipant->save();
                        }
                    }

                }

            }
        }
        Log::info('> Imported');

        /* Alle items die een 1 hebben zitten nog in de prullenbak en komen niet langer in de import voor, verwijder ze */
        DB::delete("DELETE FROM race WHERE `trash` = 1");
        DB::delete("DELETE FROM category WHERE `trash` = 1");
        DB::delete("DELETE FROM participant WHERE `trash` = 1");
        Log::info('> Cleaned');

    }

    public function importResults($year, $month)
    {
        Log::info('Import result (year: ' . $year . ' month: ' . $month . ')');

        $sUrl = 'http://api.knwu.nl/?callback=&handle=knwu.app.results.get&jaar=' . $year . '&maand=' . $month;


        if(Cache::sameAsCache($sUrl)) { //Item is exact gelijk als de vorige import, we hoeven niets te doen
            Log::info('> Skip');
            return;
        }

        $json = file_get_contents($sUrl);
        $decoded = json_decode($json);

        foreach ($decoded->result as $race) {
            if(isset($race->categorieen)) {
                foreach ($race->categorieen as $category) {
                    if (isset($category->uitslag)) {
                        /* Deelnemers */
                        foreach ($category->uitslag as $participant) {
                            $sNewHash = Participant::generateHash($participant->naam . $participant->woonplaats);

                            $oParticipant = Participant::where([
                                ['hash', '=', $sNewHash],
                                ['category_id', '=', (int)$category->categorieId]
                            ])->first();

                            if($oParticipant != null) {
                                $oParticipant->position = (int)$participant->positie;
                                $oParticipant->dnf = (boolean)$participant->dnf;
                                $oParticipant->dns = (boolean)$participant->dns;
                                $oParticipant->dq = (boolean)$participant->dq;
                                $oParticipant->raceDone = true;
                                $oParticipant->save();
                            }
                        }
                    }
                }
            }
        }

        Log::info('> Imported');

    }

    public function import()
    {
        set_time_limit(0); //Run infinity

        Log::info('* Start Race Import *');

        /* Import 6 months in past and 6 next months. */
        $sixMonthsFromNow = date('Y-m', strtotime('+6 month')) . '-01';

        /* pak 6 maanden in de toekomst en tel er 12 terug */

        for($i = 0; $i < 12; $i++)
        {
            $date = date('Y-m', strtotime($sixMonthsFromNow . '-' . $i . ' month')) . '-01';
            $year = date('Y', strtotime($date));
            $month = date('m', strtotime($date));

            $this->importRace($year, $month);
        }

        Log::info('* End Race Import *');

        Log::info('* Start Result Import *');

        $dateToday = date('Y-m-d');

        /* Bij de eerste 7 dagen van de huidige maand wordt ook vorige maand geimporteerd */
        if((int)date('d', strtotime($dateToday)) < 8) {
            $this->importResults(date('Y'), date('m', strtotime('-1 month')));
        }
        /* importeer resultaten van huidige maand */
        $this->importResults(date('Y'), date('m'));

        Log::info('* End Result Import *');

    }
}
