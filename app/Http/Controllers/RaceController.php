<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Models\Race;
use App\Models\Category;
use App\Models\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;

class RaceController extends Controller
{

    public function index() {

        $aYear = DB::select('SELECT YEAR(`startDate`) year FROM race WHERE YEAR(`startDate`) IS NOT NULL GROUP BY YEAR(`startDate`) ORDER BY YEAR(`startDate`) DESC');
        $aMonth = [
                    '01' => 'January',
                    '02' => 'February',
                    '03' => 'March',
                    '04' => 'April',
                    '05' => 'May',
                    '06' => 'June',
                    '07' => 'July',
                    '08' => 'August',
                    '09' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                  ];

        $aYearArray = [];

        foreach ($aYear as $year) {
            $aYearArray[$year->year] = $year->year;
        }

        return view('race.index', ['aYear' => $aYearArray, 'aMonth' => $aMonth]);
    }

}
