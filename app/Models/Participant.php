<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Race
 * @property string $name
 * @property string $location
 * @mixin Model
 * @package App
 */

class Participant extends Model
{
    protected $fillable = ['id', 'name', 'from', 'raceNumber', 'position', 'dnf', 'dns', 'dq', 'raceDone', 'category_id', 'trash', 'manual', 'hash'];
    protected $table = 'participant';

    public static function generateHash($input) {
        $sNewHash = preg_replace('/\s+/', '', $input);
        return md5(strtolower($sNewHash));
    }
}