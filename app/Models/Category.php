<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Participant;

/**
 * Class Race
 * @property string $name
 * @property string $location
 * @mixin Model
 * @package App
 */

class Category extends Model
{
    protected $fillable = ['id', 'name', 'participantsFrom', 'raceType', 'date', 'time', 'race_id', 'trash', 'manual'];
    protected $table = 'category';


    public function participants() {
        return Participant::where('category_id', '=', $this->id)->get();
    }

    public function participantsWithHash($array) {
        return Participant::where('category_id', '=', $this->id)->whereIn('hash', $array)->orderBy('hash')->get();
    }
}