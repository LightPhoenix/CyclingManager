<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Race
 * @property string $name
 * @property string $location
 * @mixin Model
 * @package App
 */

class Cache extends Model
{

    protected $table = 'cache';

    public static function sameAsCache($url) {

        $response = file_get_contents($url);
        $newHash = md5($response);

        $bSkip = false;

        $oCache = self::where('url', '=', $url)->first();
        if ($oCache != null) {

            if($oCache->checksum == $newHash) {
                $bSkip = true;
            } else {
                $oCache->checksum = $newHash;
                $oCache->save();
            }

        } else {
            $oCache = new self();
            $oCache->url = $url;
            $oCache->checksum = $newHash;
            $oCache->save();
        }

        return $bSkip;
    }
}