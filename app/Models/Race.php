<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

/**
 * Class Race
 * @property string $name
 * @property string $location
 * @mixin Model
 * @package App
 */

class Race extends Model
{

    protected $fillable = ['id', 'name', 'location', 'startDate', 'permanence', 'discipline', 'isCanceled', 'trash', 'manual'];
    protected $table = 'race';

    public function categories() {
        return Category::where('race_id', '=', $this->id)->get();
    }
}