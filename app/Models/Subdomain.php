<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * Class Race
 * @property string $name
 * @property string $location
 * @mixin Model
 * @package App
 */

class Subdomain extends Model
{

    protected $table = 'subdomain';

    public static function current() {
        return explode('.', explode('//', url('/'))[1])[0];
    }

    public static function currentObject() {
        return self::where('subDomain', '=', self::current())->first();
    }

    public static function getHashList() {
        $current = self::currentObject();
        return User::where(
            [
                ['subDomain_id', '=', $current->id],
                ['racerHash', '!=', 'NULL']
            ])->pluck('racerHash', 'name')->toArray();
    }

}