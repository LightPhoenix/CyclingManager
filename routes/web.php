<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AjaxController;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/import', ['as' => 'import', 'uses' => 'ImportController@import']);
Route::get('/account', ['as' => 'account.index', 'uses' => 'AccountController@index']);
Route::post('/account', ['as' => 'account.post', 'uses' => 'AccountController@post']);

Route::get('/race', ['as' => 'race.index', 'uses' => 'RaceController@index']);


/* AJAX */
$oAjax = new AjaxController();
$oAjax->buildRoutes();